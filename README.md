# simple_loss_framework

A very simplistic and experimental skeleton framework for loss risks. 
Nothing fancy, just a sandbox project.

## Install

Create a virtual environment then simply install it through:

```
pip install git+https://www.gitlab.com/v-m/simple_loss_framework
```

Or clone the repo and install it:

```
git clone https://www.gitlab.com/v-m/simple_loss_framework

python -m venv venv
source venv/bin/activate

pip install -e .\[dev\]
```

## Running tests

To run the tests, the project should be cloned.
Then, simply invoke the `pytest` command.

## Running

To run the Florida Hurricane sample model, use the `florida_hurricanes` tool.
Example:

```
Usage: florida_hurricanes [OPTIONS] FLORIDA_LANDFALL_RATE FLORIDA_MEAN
                          FLORIDA_STDDEV GULF_LANDFALL_RATE GULF_MEAN
                          GULF_STDDEV

  Model of the economic losses due to a hurricane landfall based on two sub
  models for Florida and Gulf estimation.

  The estimation is computed accrording to the followings: (i) Annual rate
  at which hurricanes make landfall in Florida assume a Poisson distribution
  defined by <florida_landfall_rate> lambda parameter. (ii) Probability of
  economic loss of a hurricane that makes landfall in Florida is defined by
  a log-normal distribution where <florida_landfall_rate> is the mu
  parameter and <florida_stddev> is the sigma parameter. (iii) Annual rate
  at which hurricanes make landfall in Gulf state assume a Poisson
  distribution defined by <gulf_landfall_rate> lambda parameter. (iv)
  Probability of economic loss of a hurricane that makes landfall in Gulf
  State is defined by a lognormal distribution where <gulf_landfall_rate> is
  the mu parameter and <gulf_stddev> is the sigma parameter.

Options:
  -n, --num_monte_carlo_samples INTEGER RANGE
  -v, --verbose                   Print debug logging
  --help                          Show this message and exit.
```