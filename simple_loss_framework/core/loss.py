"""
Defines loss estimation
"""
from abc import ABC
from dataclasses import dataclass

import numpy


class EventLossEstimator(ABC):
    """
    Abstract class defining methods for loss estimation
    """

    def draw_loss(self, **kwargs) -> float:
        """
        :param kwargs: arguments to the estimation (if needed)
        :raises MissingParametersException: a parameter is expected but missing
        :return: estimated loss
        """
        raise NotImplementedError


@dataclass(frozen=True)
class LogNormalEventLossEstimator(EventLossEstimator):
    """
    Simple loss estimator based on a log-normal distribution
    """

    mean: float
    std_dev: float

    def draw_loss(self, **kwargs) -> int:
        return numpy.random.lognormal(self.mean, self.std_dev)
