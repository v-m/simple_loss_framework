from unittest.mock import MagicMock

import pytest

from simple_loss_framework.core.exceptions import (
    NbSampleException,
    NoModelsException,
)
from simple_loss_framework.core.simulation import LossSimulation


@pytest.mark.parametrize(
    "samples", [0, -1],
)
def test_samples_input_error(samples):
    with pytest.raises(NbSampleException):
        LossSimulation(samples, MagicMock())


def test_no_model_error():
    with pytest.raises(NoModelsException):
        LossSimulation(100)
