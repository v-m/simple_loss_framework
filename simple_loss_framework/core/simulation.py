"""
Loss simulation drive the whole simulation process.
"""

import itertools
import logging
import statistics
from typing import List

from simple_loss_framework.core.exceptions import (
    NbSampleException,
    NoModelsException,
)
from simple_loss_framework.core.models import Model
from simple_loss_framework.core.timeit import time_monitor

LOGGER = logging.getLogger(__file__)


class LossSimulation:
    """
    Defines a model as a number of samples and a list of models
    to run through for running one iteration of the model.
    """

    def __init__(self, samples: int, *models: Model):
        """
        :param samples: the number of samples for this simulation
        :param models: the list of models which compose this simulation
        :raises NbSampleException: if samples is <= 0
        :raises NoModelsException: if no model is passed
        """
        if samples < 1:
            raise NbSampleException(
                "Expect a positive non null number of samples"
            )
        if not any(models):
            raise NoModelsException

        self._samples = samples
        self._models = models
        LOGGER.debug(
            "Initializing Simulation. Sample size = %d. Models = %s",
            self._samples,
            self._models,
        )

    def _run_iteration(self) -> List[List[float]]:
        return [list(model.estimate_loss()) for model in self._models]

    @time_monitor
    def run(self):
        """
        Run the whole simulation
        """
        LOGGER.debug("Running model.")

        runs = [self._run_iteration() for _ in range(self._samples)]
        LOGGER.debug("Runs result: %s", runs)

        runs = list(map(lambda x: sum(itertools.chain(*x)), runs))
        LOGGER.debug("Runs result – sums: %s", runs)

        return statistics.mean(runs)
