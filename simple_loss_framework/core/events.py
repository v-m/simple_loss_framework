"""
Define event occurrence
"""

from abc import ABC
from dataclasses import dataclass

import numpy


class EventOccurrenceEstimator(ABC):
    """
    Abstract class defining how we can define the event occurrence logic
    """

    def draw_event_occurrence(self, **kwargs) -> int:
        """
        :param kwargs: arguments to the estimation (if needed)
        :raises MissingParametersException: a parameter is expected but missing
        :return: a number of event occurrence
        """
        raise NotImplementedError


@dataclass(frozen=True)
class PoissonOccurrenceEstimator(EventOccurrenceEstimator):
    """
    Simple occurrence estimator based on a poisson distribution
    """

    rate: float

    def draw_event_occurrence(self, **kwargs) -> int:
        return numpy.random.poisson(self.rate)
