"""
Models definitions
"""

import logging
from abc import ABC
from dataclasses import dataclass
from typing import Iterable

from simple_loss_framework.core.events import EventOccurrenceEstimator
from simple_loss_framework.core.loss import EventLossEstimator

LOGGER = logging.getLogger(__file__)


class Model(ABC):
    """
    Abstract class defining any model
    """

    def estimate_loss(self) -> Iterable[float]:
        """
        Estimate the loss for the model
        """
        raise NotImplementedError


@dataclass(frozen=True)
class NrEventsWithLossModel(Model):
    """
    Simple model based on an occurrence estimator and a loss estimator
    """

    event_occurrence_estimator: EventOccurrenceEstimator
    event_loss_estimator: EventLossEstimator

    def estimate_loss(self) -> Iterable[float]:
        drawn_nb_occurrences = (
            self.event_occurrence_estimator.draw_event_occurrence()
        )
        LOGGER.debug("Drawn nb occurrences = %d", drawn_nb_occurrences)

        for _ in range(drawn_nb_occurrences):
            drawn_value = self.event_loss_estimator.draw_loss()
            LOGGER.debug("Drawn value = %f", drawn_value)
            yield drawn_value
