"""
Project exceptions
"""


class NbSampleException(Exception):
    """Requested samples is not an acceptable value"""

    pass


class NoModelsException(Exception):
    """No model are defined for the simulation"""

    pass


class MissingParametersException(Exception):
    """
    Raised by a model or estimator if
    it require parameters and they are missing
    """
