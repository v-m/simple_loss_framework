"""
Function monitoring helpers
"""
import functools
import logging
import time

LOGGER = logging.getLogger(__file__)


def time_monitor(func):
    """Decorator used to compute a function execution time"""

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time_ns()
        value = func(*args, **kwargs)
        end_time = time.time_ns()

        LOGGER.info(
            "Execution time for %s: %d ns",
            func.__qualname__,
            end_time - start_time,
        )

        return value

    return wrapper
