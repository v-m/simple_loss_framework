from unittest.mock import MagicMock

import pytest

from simple_loss_framework.core.models import NrEventsWithLossModel


@pytest.mark.parametrize(
    "occurrences, value, result",
    [(0, 1, []), (5, 1, [1, 1, 1, 1, 1]), (1, 1, [1])],
)
def test_nr_events_with_loss_model(occurrences, value, result):
    event_occurrence_estimator = MagicMock()
    event_occurrence_estimator.draw_event_occurrence.return_value = occurrences
    event_loss_estimators = MagicMock()
    event_loss_estimators.draw_loss.return_value = value

    assert (
        list(
            NrEventsWithLossModel(
                event_occurrence_estimator, event_loss_estimators
            ).estimate_loss()
        )
        == result
    )
