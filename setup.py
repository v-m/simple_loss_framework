import setuptools

setuptools.setup(
    python_requires=">=3.7",
    name="simple_loss_framework",
    version="0.0.1",
    url="https://gitlab.com/v-m/simple_loss_framework",
    author="Vincenzo Musco",
    author_email="muscovin@gmail.com",
    description="Simple Loss Framework",
    packages=setuptools.find_packages(exclude=["tests"]),
    install_requires=["numpy", "click"],
    extras_require={"dev": ["pytest", "black", "flake8"]},
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "florida_hurricanes = simple_loss_framework.models.florida_hurricanes:main",
        ]
    },
    include_package_data=True,
    zip_safe=False,
)
