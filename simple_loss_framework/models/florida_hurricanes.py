"""
Entry point for the florida hurricane model
"""
import logging

import click

from simple_loss_framework.core.events import PoissonOccurrenceEstimator
from simple_loss_framework.core.loss import LogNormalEventLossEstimator
from simple_loss_framework.core.models import NrEventsWithLossModel
from simple_loss_framework.core.simulation import LossSimulation


@click.command()
@click.argument("florida-landfall-rate", type=click.FloatRange())
@click.argument("florida-mean", type=click.FloatRange())
@click.argument("florida-stddev", type=click.FloatRange())
@click.argument("gulf-landfall-rate", type=click.FloatRange())
@click.argument("gulf-mean", type=click.FloatRange())
@click.argument("gulf-stddev", type=click.FloatRange())
@click.option(
    "-n", "--num_monte_carlo_samples", type=click.IntRange(1), default=100
)
@click.option(
    "-v", "--verbose", is_flag=True, default=False, help="Print debug logging"
)
def florida_loss_model(
        florida_landfall_rate,
        florida_mean,
        florida_stddev,
        gulf_landfall_rate,
        gulf_mean,
        gulf_stddev,
        num_monte_carlo_samples=100,
        verbose=False,
):
    """
    Model of the economic losses due to a hurricane landfall
    based on two sub models for Florida and Gulf estimation.

    The estimation is computed accrording to the followings:
    (i) Annual rate at which hurricanes make landfall in
    Florida assume a Poisson distribution defined
    by <florida_landfall_rate> lambda parameter.
    (ii) Probability of economic loss of a hurricane that
    makes landfall in Florida is defined by a log-normal
    distribution where <florida_landfall_rate> is the mu
    parameter and <florida_stddev> is the sigma parameter.
    (iii) Annual rate at which hurricanes make landfall
    in Gulf state assume a Poisson distribution defined
    by <gulf_landfall_rate> lambda parameter.
    (iv) Probability of economic loss of a hurricane that
    makes landfall in Gulf State is defined by a lognormal
    distribution where <gulf_landfall_rate> is the mu
    parameter and <gulf_stddev> is the sigma parameter.
    """
    # pylint: disable=too-many-arguments

    if verbose:
        logging.basicConfig(level="DEBUG")

    florida = NrEventsWithLossModel(
        PoissonOccurrenceEstimator(florida_landfall_rate),
        LogNormalEventLossEstimator(florida_mean, florida_stddev),
    )
    gulf = NrEventsWithLossModel(
        PoissonOccurrenceEstimator(gulf_landfall_rate),
        LogNormalEventLossEstimator(gulf_mean, gulf_stddev),
    )
    simulation = LossSimulation(num_monte_carlo_samples, florida, gulf)

    print(simulation.run())


def main():
    """
    Main function
    """
    # pylint: disable=no-value-for-parameter
    florida_loss_model()


if __name__ == "__main__":
    main()
